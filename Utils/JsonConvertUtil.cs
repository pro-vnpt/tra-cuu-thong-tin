﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tra_cuu_thong_tin.Entity;

namespace Tra_cuu_thong_tin.Utils
{
    /// <summary>
    /// Lớp xử lý các hành động chuyển dữ liệu giữa Object và String
    /// </summary>
    public static class JsonConvertUtil
    {
        public static readonly DateFormatHandling JSONDateFormatHandling = DateFormatHandling.IsoDateFormat;
        public static readonly DateTimeZoneHandling JSONDateTimeZoneHandling = DateTimeZoneHandling.Local;
        public static readonly string JSONDateFormatString = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffFFFFK";
        public static readonly NullValueHandling JSONNullValueHandling = NullValueHandling.Include;
        public static readonly ReferenceLoopHandling JSONReferenceLoopHandling = ReferenceLoopHandling.Ignore;

        /// <summary>
        /// Custom lại format
        /// </summary>
        /// <returns></returns>
        public static JsonSerializerSettings GetJsonSerializerSetting()
        {
            return new JsonSerializerSettings()
            {
                DateFormatHandling = JSONDateFormatHandling,
                DateTimeZoneHandling = JSONDateTimeZoneHandling,
                DateFormatString = JSONDateFormatString,
                NullValueHandling = JSONNullValueHandling,
                ReferenceLoopHandling = JSONReferenceLoopHandling
            };
        }

        /// <summary>
        /// Hàm chuyển object sang json
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SerializeObject(object data)
        {
            return JsonConvert.SerializeObject(data, GetJsonSerializerSetting());
        }

        /// <summary>
        /// Hàm chuyển json sang object thuộc kiểu T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data, GetJsonSerializerSetting());
        }

        /// <summary>
        /// Hàm chuyển json sang object
        /// </summary>
        /// <param name="data"></param>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public static object DeserializeObject(string data, Type objectType)
        {
            return JsonConvert.DeserializeObject(data, objectType, GetJsonSerializerSetting());
        }

        /// <summary>
        /// Hàm convert từ string Json sang object hứng API responsse
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<T> DeserializeListObject<T>(string data)
        {
            List<T> lstGeneric = null;
            if (!string.IsNullOrWhiteSpace(data))
            {
                try
                {
                    var dataObj = JsonConvert.DeserializeObject<APIExternal<T>>(data);
                    lstGeneric = dataObj.data;
                }
                catch (Exception)
                {
                }
            }

            // MakeGenericType để truyền vào Type của một list Generic, không thể khởi tạo trực tiếp typeof(List<T>) được
            //lstRes = JsonConvertUtil.DeserializeObject(strData, typeof(List<>).MakeGenericType(new[] { typeof(T) } )) as List<T>;
            return lstGeneric;
        }
    }
}
