﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Tra_cuu_thong_tin.Utils
{
    public class HttpClientUtil
    {
        public static async Task<string> callAPILoginCCBS(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            client.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0)");
            client.DefaultRequestHeaders.Add("Referer", "http://ccbs.vnpt.vn/ccbs/login.htm");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded")
            };
            response = await client.SendAsync(request);
            var loginResponseString = await response.Content.ReadAsStringAsync();
            string cookie = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
            client.DefaultRequestHeaders.Remove("Referer");
            client.DefaultRequestHeaders.Add("Cookie", cookie + "; BIGipServerAPP_CCBS=1578540810.20480.0000");
            return loginResponseString;
        }


        public static async Task<string> callAPICCBS(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = null
            };
            response = await client.SendAsync(request);
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }

        public static async Task<string> callAPI(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = content != null ? new StringContent(content, Encoding.UTF8, "application/json") : null
            };
            try
            {
                response = await client.SendAsync(request);
            }
            catch (Exception)
            {
            }
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }

        public static async Task<string> verifyOTP(HttpClient client, string url, string rawContent)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var content = new StringContent(rawContent);
            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
            try
            {
                response = await client.PostAsync(url, content);
            }
            catch (Exception)
            {
            }
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }

        public static async Task<string> checkTokenAlive(HttpClient client, string url)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            try
            {
                response = await client.GetAsync(url);
            }
            catch (Exception)
            {
            }
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }
    }
}
