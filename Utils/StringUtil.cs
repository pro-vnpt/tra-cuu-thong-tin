﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tra_cuu_thong_tin.Utils
{
    internal class StringUtil
    {
        public static string extractFromTo(string data, string from, string to, int index_need_to_get)
        {
            string response = "";
            string regexString = string.Format("(?<={0}).*?(?={1})", from, to);
            var res = Regex.Matches(data, regexString, RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                response = res[index_need_to_get].ToString();
            }
            return response;
        }

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                int Start, End;
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }

            return "";
        }

        public static string getBetweenByRegex(string data, string regex, int index_need_to_get)
        {
            string output = "";
            // @"(?<={from}).*?(?={to})"
            // nếu {to} là 1 dấu "
            // thì phải thêm ""

            var res = Regex.Matches(data, regex, RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                output = res[index_need_to_get].ToString();
            }
            return output;
        }

        public static string formatString(string str, bool isCurrency = false)
        {
            return string.IsNullOrWhiteSpace(str) ? (isCurrency ? "0" : "N/A") : str;
        }

        public static string Right(string param, int length)
        {
            return param.Substring(param.Length - length, length);
        }
        /// <summary>
        /// Hàm lấy số Luhn
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static int getLuhnCheckDigit(string number)
        {
            int sum = 0;
            bool alt = true;
            char[] digits = number.ToCharArray();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int curDigit = digits[i] - 48;
                if (alt)
                {
                    curDigit *= 2;
                    if (curDigit > 9)
                    {
                        curDigit -= 9;
                    }
                }
                sum += curDigit;
                alt = !alt;
            }
            if (sum % 10 == 0)
            {
                return 0;
            }
            return 10 - sum % 10;
        }
        public static string getMsinNumber10(string imsi)
        {

            string msin_full = "";
            if (!string.IsNullOrWhiteSpace(imsi))
            {
                string msin = Right(imsi.Trim(), 10);
                int iLuhn = getLuhnCheckDigit(msin) + 1;
                if (iLuhn % 10 == 0)
                {
                    iLuhn = 0;
                }
                msin_full = "898402000" + msin + iLuhn;
            }
            return msin_full;
        }

        public static string formatPhone84(string phone)
        {
            string _phone = phone.Trim().Replace(".", "");
            if (_phone.StartsWith("0")) _phone = _phone.Substring(1);
            if ((_phone.StartsWith("84") && _phone.Length < 10) || !_phone.StartsWith("84"))
            {
                _phone = "84" + _phone;
            }
            return _phone;
        }
    }
}
