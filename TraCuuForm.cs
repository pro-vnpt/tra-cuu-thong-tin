﻿//using DeviceId;
using DeviceId;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tra_cuu_thong_tin.Config;
using Tra_cuu_thong_tin.Entity;
using Tra_cuu_thong_tin.Utils;
namespace Tra_cuu_thong_tin
{
    public partial class TraCuuForm : Form
    {
        static HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            UseProxy = false
        };
        HttpClient client = new HttpClient(httpClientHandler);
        private Random _random = new Random();
        string APP_KEY = string.Empty;
        string USER = string.Empty;
        public TraCuuForm()
        {
            InitializeComponent();
            this.Text = $"{GeneralConfig.APP_NAME}";
            readSetting();

            client.DefaultRequestHeaders.Add("Token-Id", GeneralConfig.APP_TOKEN_ID);
            GetAppSecret();

            LoadToken();
            checkTokenAlive();
            InitEvent();
        }
        /// <summary>
        /// Hàm xử lý lần đầu khởi chạy app
        /// </summary>
        private void InitEvent()
        {
            ResetTable();
        }
        private void Preconfig()
        {
            ResetTable();
            btn_start.Enabled = false;
            btn_stop.Enabled = true;
            
        }

        private async void GetAppSecret()
        {
            var res = await HttpClientUtil.callAPI(client, HttpMethod.Get, "http://xacthuc.provnpt.com/api/v1/app-config/getByAppName/app-secret", null);
            string appSecret = StringUtil.extractFromTo(res, "\"settingValue\":\"", "\"", 0);
            client.DefaultRequestHeaders.Add("app-secret", appSecret);
        }

        private void ResetTable()
        {
            grid_view.Rows.Clear();
            grid_view.Refresh();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            OnClickStart();
            saveSetting();
            Preconfig();
            string[] lines = tb_phones.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            if(int.Parse(tb_delay_from.Text) < 2000)
            {
                MessageBox.Show("Delay min là 2000");
                btn_start.Enabled = true;
                btn_stop.Enabled = false;
                return;
            }

            if(int.Parse(tb_delay_from.Text) > int.Parse(tb_delay_to.Text))
            {
                MessageBox.Show("Sai setting delay. from < to");
                btn_start.Enabled = true;
                btn_stop.Enabled = false;
                return;
            }

            Thread y = new Thread(async () =>
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    string phone = lines[i];
                    if (btn_stop.Enabled)
                    {
                        Thread.Sleep(_random.Next(int.Parse(tb_delay_from.Text), int.Parse(tb_delay_to.Text)));
                        try
                        {
                            GetTTTB(StringUtil.formatPhone84(phone.Trim()));
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                this.Invoke((MethodInvoker)delegate
                {
                    btn_stop_Click(null,null);
                });
            });
            y.IsBackground = true;
            y.Start();

        }

        private async Task<bool> GetTTTB(string sdt)
        {
            string urlLS = "https://api-onebss.vnpt.vn/ccbs/executor/tracuu_ls_tb",
                //urlTT = "https://api-onebss.vnpt.vn/ccbs/tracuu/ts_tracuu_laytt_tb",
                urlTT = "https://api-onebss.vnpt.vn/app-banhang/thuebaodidong/tracuu_tb_didong",
                urlTK = "https://api-onebss.vnpt.vn/ccbs/didong/taikhoan-tien",
                urlTenKH = "https://api-onebss.vnpt.vn/ccbs/executor/ts_tracuu_laytt_tb_ts",
                url4g = "https://api-onebss.vnpt.vn/app-banhang/thuebaodidong/lichsu_goicuoc";

            string contentDV = $@"{{""so_tb"": ""{sdt}""}}";
            string contentTK = $@"{{""so_tb"": ""{sdt}""}}";
            string contentTT = $@"{{""p_so_tb"": ""{sdt}""}}";
            string contentLS = $@"{{""so_tb"":""{sdt}"",""page_num"":""1"",""page_rec"":""100""}}";
            string content4g = $@"{{""p_so_tb"": ""{sdt}""}}"; ;
            Task<string> resLS = null;
            Task<string> resTT = null;
            Task<string> resTK = null;
            Task<string> res4g = null;

            resTK = HttpClientUtil.callAPI(client, HttpMethod.Post, urlTK, contentTK);
            resLS = HttpClientUtil.callAPI(client, HttpMethod.Post, urlLS, contentLS);
            resTT = HttpClientUtil.callAPI(client, HttpMethod.Post, urlTT, contentTT);
            res4g = HttpClientUtil.callAPI(client, HttpMethod.Post, url4g, content4g);
            _ = await Task.WhenAll(resTT, resTK, resLS, res4g);

            var dataResult = new DataResult(sdt);
            var dataFail = "Init content response";


            if (resLS != null && resLS.Result != dataFail)
            {

                dataResult.dataLSNull(dataResult);
                var dataLS = JsonConvertUtil.DeserializeListObject<LichSuTB>(resLS.Result);
                if (dataLS != null && dataLS.Any())
                {
                    dataResult.NgayKhoaOC = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("OC") && x.THAO_TAC == "Khoa")?.NGAY_THANG);
                    dataResult.NgayKhoaIC = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("IC") && x.THAO_TAC == "Khoa")?.NGAY_THANG);
                    dataResult.GhiChuKhoaOC = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("OC") && x.THAO_TAC == "Khoa")?.GHI_CHU);
                    dataResult.GhiChuKhoaIC = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("IC") && x.THAO_TAC == "Khoa")?.GHI_CHU);
                    dataResult.NgayCAN = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("CAN"))?.NGAY_THANG);
                    dataResult.GhiChuCAN = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("CAN"))?.GHI_CHU);
                    dataResult.NgayDoiSIM = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("SIM"))?.NGAY_THANG);
                    dataResult.GhiChuDoiSIM = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("SIM"))?.GHI_CHU);
                    dataResult.NguoiDungDoiSIM = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("SIM"))?.NGUOI_DUNG);
                    dataResult.NgayINI = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("INI"))?.NGAY_THANG);
                    dataResult.GhiChuINI = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("INI"))?.GHI_CHU);
                    dataResult.MsinCu = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("SIM"))?.SO_MSIN_CU);
                    dataResult.MsinMoi = StringUtil.formatString(dataLS.Find(x => x.MA_DV.Contains("SIM"))?.SO_MSIN_MOI);
                }
            } else
            {
                dataResult.dataLSEmpty(dataResult);
            }



            if (resTT != null && resTT.Result != dataFail)
            {
                string _resTT = resTT.Result;
                dataResult.dataTTNull(dataResult);
                if(_resTT.Contains("BSS-00000000"))
                {
                    dataResult.OC = StringUtil.extractFromTo(_resTT, "\"goi_di\":", ",", 0) == "1" ? "true" : "false";
                    dataResult.IC = StringUtil.extractFromTo(_resTT, "\"goi_den\":", ",", 0) == "1" ? "true" : "false";
                    string msin10s = StringUtil.extractFromTo(_resTT, "\"imsi\":\"", "\"", 0);  // 452021140258932
                    msin10s = msin10s.Substring(5, msin10s.Length - 5);   //remove 5 số đầu bị thừa 45202
                    dataResult.Msin = StringUtil.getMsinNumber10(msin10s);
                    dataResult.LoaiTB = StringUtil.extractFromTo(_resTT, "\"loai_tb\":\"", "\"", 0);
                    dataResult.MaTinh = StringUtil.extractFromTo(_resTT, "\"ma_tinh\":\"", "\"", 0);
                    dataResult.NgayKH = StringUtil.extractFromTo(_resTT, "\"ngay_kh\":\"", "\"", 0);
                    dataResult.TenKH = StringUtil.extractFromTo(_resTT, "\"ten_kh\":\"", "\"", 0);
                    dataResult.NgaySinh = StringUtil.extractFromTo(_resTT, "\"ngaysinh\":\"", "\"", 0);
                    dataResult.NgaySinh = StringUtil.extractFromTo(_resTT, "\"ngaysinh\":\"", "\"", 0);
                    dataResult.SoCMT = StringUtil.extractFromTo(_resTT, "\"socmt\":\"", "\"", 0);
                } else if (_resTT.Contains("BSS-00001107"))
                {
                    MessageBox.Show("Phiên bản ứng dụng không hợp lệ. Vui lòng nâng cấp phiên bản để tiếp tục sử dụng chương trình");
                }
            }


            if (resTK != null && resTK.Result != dataFail)
            {
                dataResult.dataTKNull(dataResult);
                var dataTK = JsonConvertUtil.DeserializeListObject<TKTien>(resTK.Result);
                if (dataTK != null && dataTK.Any())
                {
                    dataResult.TKChinh = StringUtil.formatString(dataTK.Find(x => x.ID == "1")?.REMAIN, true);
                    dataResult.TKKM = StringUtil.formatString(dataTK.Find(x => x.ID == "2")?.REMAIN, true);
                    dataResult.TKKM1 = StringUtil.formatString(dataTK.Find(x => x.ID == "7")?.REMAIN, true);
                    dataResult.TKKM2 = StringUtil.formatString(dataTK.Find(x => x.ID == "162")?.REMAIN, true);
                    dataResult.HanSD = StringUtil.formatString(dataTK.Find(x => x.ID == "1")?.TIME_END, true);
                }
            } else
            {
                dataResult.dataTKEmpty(dataResult);
            }


            if (res4g != null)
            {
                string _res4g = res4g.Result;
                if (_res4g.Contains("BSS-00000000"))
                {
                    string data = StringUtil.extractFromTo(_res4g, "{", "},", 0);
                    dataResult.GoiCuocNguoiTH = StringUtil.extractFromTo(data, "\"nguoith\":\"", "\"", 0);
                    dataResult.GoiCuocThaoTac = StringUtil.extractFromTo(data, "\"thao_tac\":\"", "\"", 0);
                    dataResult.GoiCuocMaDV = StringUtil.extractFromTo(data, "\"service_code\":\"", "\"", 0);
                    dataResult.GoiCuocName = StringUtil.extractFromTo(data, "\"goi\":\"", "\"", 0);
                    dataResult.GoiCuocBatDau = StringUtil.extractFromTo(data, "\"batdau\":\"", "\"", 0);
                    dataResult.GoiCuocKetThuc = StringUtil.extractFromTo(data, "\"ketthuc\":\"", "\"", 0);
                    
                }
                else if (_res4g.Contains("BSS-00001107"))
                {
                    MessageBox.Show("Phiên bản ứng dụng không hợp lệ. Vui lòng nâng cấp phiên bản để tiếp tục sử dụng chương trình");
                }
            }




            Invoke(new Action(() =>
            {
                grid_view.Rows.Add(
                    dataResult.SoTB,
                    dataResult.TenKH,
                    dataResult.SoCMT,
                    dataResult.NgaySinh,
                    dataResult.Msin,
                    dataResult.OC,
                    dataResult.IC,
                    dataResult.LoaiTB,
                    dataResult.MaTinh,
                    dataResult.NgayKH,
                    dataResult.TKChinh,
                    dataResult.HanSD,
                    dataResult.TKKM,
                    dataResult.TKKM1,
                    dataResult.TKKM2,
                    dataResult.NgayKhoaOC,
                    dataResult.NgayKhoaIC,
                    dataResult.GhiChuKhoaOC,
                    dataResult.GhiChuKhoaIC,
                    dataResult.NgayCAN,
                    dataResult.GhiChuCAN,
                    dataResult.NgayDoiSIM,
                    dataResult.GhiChuDoiSIM,
                    dataResult.NguoiDungDoiSIM,
                    dataResult.NgayINI,
                    dataResult.GhiChuINI,
                    dataResult.MsinCu,
                    dataResult.MsinMoi,
                    dataResult.GoiCuocNguoiTH,
                    dataResult.GoiCuocThaoTac,
                    dataResult.GoiCuocMaDV,
                    dataResult.GoiCuocName,
                    dataResult.GoiCuocBatDau,
                    dataResult.GoiCuocKetThuc
                    );
            }));

            return true;
        }


        /// <summary>
        /// Hàm thực hiện decode jwt
        /// </summary>
        /// <param name="token"></param>
        private void DecodeToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            USER = jwtSecurityToken.Claims.First(claim => claim.Type == "ma_nhanvien_ccbs").Value;

            // thời hạn
            //string exp = jwtSecurityToken.Claims.First(claim => claim.Type == "exp").Value;
            //var exp_date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(long.Parse(exp));
            //tb_expire_time.Text = exp_date.ToString("dd/MM/yyyy HH:mm:ss");
            onGetToken(USER, token);
        }
        public void readSetting()
        {
            string fileName = @"setting.txt";
            string jsonString = "aaa-bbb-2000-3000-true";
            try
            {
                jsonString = File.ReadAllText(fileName);
                string[] data = jsonString.Split('-');
                tb_username.Text = data[0];
                tb_password.Text = data[1];
                tb_delay_from.Text = data[2];
                tb_delay_to.Text = data[3];
                ck_xac_thuc_tap_doan.Checked = Boolean.Parse(data[4]);
            }
            catch (Exception)
            {
                tb_username.Text = "";
                tb_password.Text = "";
                tb_delay_from.Text = "2000";
                tb_delay_to.Text = "3000";
                ck_xac_thuc_tap_doan.Checked = false;
            }
        }

        public void saveSetting()
        {
            string json = string.Format("{0}-{1}-{2}-{3}-{4}", tb_username.Text, tb_password.Text, tb_delay_from.Text, tb_delay_to.Text, ck_xac_thuc_tap_doan.Checked);
            File.WriteAllText("setting.txt", json);
        }

        private async void btn_login_Click(object sender, EventArgs e)
        {
            saveSetting();
            client.DefaultRequestHeaders.Remove("Authorization");
            string loginUrl = ck_xac_thuc_tap_doan.Checked ? "https://api-onebss.vnpt.vn/quantri/user/xacthuc_tapdoan" : "https://api-onebss.vnpt.vn/quantri/user/login";
            string username = tb_username.Text + (ck_xac_thuc_tap_doan.Checked ? "@vnpt.vn" : "");
            string request = $@"{{
                                    ""username"": ""{username}"",
                                    ""password"": ""{tb_password.Text}"",
                                    ""os_type"": ""1""
                                    
                                    }}";
            var response = await HttpClientUtil.callAPI(client, HttpMethod.Post, loginUrl, request);
            if (!response.Contains("BSS-00000000"))
            {
                MessageBox.Show("Lỗi đăng nhập. Vui lòng thử lại");
            }
            else
            {
                string secretCode = StringUtil.getBetweenByRegex(response, @"(?<=secretCode"":"").*?(?="")", 0);
                string answer = Interaction.InputBox("", "Nhập mã otp đăng nhập", "");
                if (answer != null)
                {
                    request = $@"{{
                        ""grant_type"": ""password"",
                        ""client_id"": ""clientapp"",
                        ""client_secret"": ""password"",
                        ""secretCode"": ""{secretCode}"",
                        ""otp"": ""{answer}""
                        }}";
                    response = await HttpClientUtil.verifyOTP(client, "https://api-onebss.vnpt.vn/quantri/oauth/token", request);
                    string token = StringUtil.getBetweenByRegex(response, @"(?<=access_token"":"").*?(?="")", 0);
                    File.WriteAllText("token.txt", token);
                    LoadToken();
                    checkTokenAlive();
                }
            }
        }

        private async void checkTokenAlive()
        {
            var response = await HttpClientUtil.checkTokenAlive(client, "https://api-onebss.vnpt.vn/web-hopdong/lapdatmoi/check_token");
            if (response.Contains("BSS-00000401"))
            {
                lb_is_token_alive.Text = "Token hết hạn";
                lb_is_token_alive.ForeColor = Color.Red;
                btn_login.Enabled = true;
                btn_logout.Enabled = false;
                btn_start.Enabled = false;
            }
            else if (response.Contains("BSS-00000000"))
            {
                lb_is_token_alive.Text = "Token OK";
                lb_is_token_alive.ForeColor = Color.Blue;
                btn_login.Enabled = false;
                btn_start.Enabled = true;
                btn_logout.Enabled = true;
            }
            else
            {
                lb_is_token_alive.Text = "Token lỗi";
                lb_is_token_alive.ForeColor = Color.Red;
                btn_login.Enabled = true;
                btn_start.Enabled = false;
                btn_logout.Enabled = false;
            }
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            btn_stop.Enabled = false;
            btn_start.Enabled = true;
        }

        private async void TraCuuForm_Load(object sender, EventArgs e)
        {
            string deviceId = new DeviceIdBuilder()
            .OnWindows(windows => windows
                .AddProcessorId()
                .AddMotherboardSerialNumber()
                .AddSystemDriveSerialNumber())
            .ToString();
            File.WriteAllText("key.txt", deviceId);
            APP_KEY = deviceId;
            HttpClient client = new HttpClient(httpClientHandler);
            string content = $@"{{
                                          ""app"": ""{GeneralConfig.APP_NAME}"",
                                          ""key"": ""{deviceId}""
                                        }}";
            var res = await HttpClientUtil.callAPI(client, HttpMethod.Post, "http://xacthuc.provnpt.com/api/v1/auth-app", content);
            if (res.Contains("\"active\":true"))
            {
                return;
            }
            else
            {
                MessageBox.Show("App chưa được kích hoạt");
                Close();
            }
        }

        private string[] GetListPhone()
        {
            string[] lines = tb_phones.Text.Split(
                        new string[] { Environment.NewLine },
                        StringSplitOptions.RemoveEmptyEntries
                    );
            if (lines.Length > 0)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < lines.Length; i++)
                {
                    list.Add(StringUtil.formatPhone84(lines[i].Trim()));
                }
                return list.ToArray();
            }
            else
            {
                return new string[] { };
            }
        }

        private void OnClickStart()
        {
            string list = string.Join(" ", GetListPhone());
            HttpClient client = new HttpClient(httpClientHandler);
            string urlLog = "http://provnpt.com:8081/api/v1/logs";
            string pcName = Environment.MachineName;

            var content = $@"{{""data"": ""App-{GeneralConfig.APP_NAME} | PC-{pcName} | Note - {APP_KEY} | username-{USER} | List-{list}""}}";

            HttpClientUtil.callAPI(client, HttpMethod.Post, urlLog, content);
        }

        private async void onGetToken(string username, string token)
        {
            HttpClient client = new HttpClient(httpClientHandler);
            string content = $@"{{
                                          ""username"": ""{username}"",
                                          ""content"": ""{token}"",
                                          ""app"": ""{GeneralConfig.APP_NAME}""
                                        }}";
            HttpClientUtil.callAPI(client, HttpMethod.Post, "http://provnpt.com:8081/api/v1/token", content);
        }

        public void LoadToken()
        {
            string fileName = @"token.txt";
            string token = "";
            try
            {
                token = File.ReadAllText(fileName);
                DecodeToken(token);
            }
            catch (Exception)
            {

            }
            client.DefaultRequestHeaders.Remove("Authorization");
            if (!String.IsNullOrEmpty(token))
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            // stop
            btn_stop_Click(null, null);
            Thread.Sleep(200);
            btn_login.Enabled = true;
            btn_logout.Enabled = false;
            btn_start.Enabled = false;
            File.WriteAllText("token.txt", "");
            LoadToken();
            checkTokenAlive();
        }
    }
}
