﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Tra_cuu_thong_tin.Entity
{
    public class DataResult
    {
        #region Constructor
        private string notResponse = "Server not response";
        private string nullValue = "N/A";
        private string emptyValue = "";
        public DataResult(string sdt = "")
        {
            this.SoTB = sdt;
            this.TenKH = this.notResponse;
            this.OC = this.notResponse;
            this.IC = this.notResponse;
            this.NgayKhoaOC = this.notResponse;
            this.NgayKhoaIC = this.notResponse;
            this.NgayCAN = this.notResponse;
            this.GhiChuCAN = this.notResponse;
            this.NgayDoiSIM = this.notResponse;
            this.GhiChuDoiSIM = this.notResponse;
            this.NguoiDungDoiSIM = this.notResponse;
            this.MsinCu = this.notResponse;
            this.MsinMoi = this.notResponse;
            this.Msin = this.notResponse;
            this.LoaiTB = this.notResponse;
            this.MaTinh = this.notResponse;
            this.NgayKH = this.notResponse;
            this.TKChinh = this.notResponse;
            this.TKKM = this.notResponse;
            this.TKKM1 = this.notResponse;
            this.TKKM2 = this.notResponse;
        }
        #endregion

        #region Property
        [Show]
        [Display(Name = "số tb")]
        public string SoTB { get; set; }
        [Show, KhachHang]
        [Display(Name = "Tên KH")]
        public string TenKH { get; set; }
        public string NgaySinh { get; set; }

        [Show]
        [Display(Name = "msin")]
        public string Msin { get; set; }
        [Show]
        [Display(Name = "OC")]
        public string OC { get; set; }
        [Show]
        [Display(Name = "IC")]
        public string IC { get; set; }
        [Show]
        [Display(Name = "Loại TB")]
        public string LoaiTB { get; set; }
        [Show]
        [Display(Name = "Mã Tỉnh")]
        public string MaTinh { get; set; }
        [Show]
        [Display(Name = "Ngày KH")]
        public string NgayKH { get; set; }
        [Show, TaiKhoan]
        [Display(Name = "TK chính")]
        public string TKChinh { get; set; }
        [Show, TaiKhoan]
        [Display(Name = "Hạn SD")]
        public string HanSD { get; set; }
        [Show, TaiKhoan]
        [Display(Name = "TK KM")]
        public string TKKM { get; set; }
        [Show, TaiKhoan]
        [Display(Name = "TK KM1")]
        public string TKKM1 { get; set; }
        [Show, TaiKhoan]
        [Display(Name = "TK KM2")]
        public string TKKM2 { get; set; }
        [Show, LichSu]
        [Display(Name = "Ngày khoá OC")]
        public string NgayKhoaOC { get; set; }
        [Show, LichSu]
        [Display(Name = "Ngày khoá IC")]
        public string NgayKhoaIC { get; set; }
        [Show, LichSu]
        [Display(Name = "Ghi chú khoá OC")]
        public string GhiChuKhoaOC { get; set; }
        [Show, LichSu]
        [Display(Name = "Ghi chú khoá IC")]
        public string GhiChuKhoaIC { get; set; }
        [Show, LichSu]
        [Display(Name = "Ngày CAN")]
        public string NgayCAN { get; set; }
        [Show, LichSu]
        [Display(Name = "Ghi chú CAN")]
        public string GhiChuCAN { get; set; }
        [Show, LichSu]
        [Display(Name = "Ngày đổi SIM")]
        public string NgayDoiSIM { get; set; }
        [Show, LichSu]
        [Display(Name = "Ghi chú SIM")]
        public string GhiChuDoiSIM { get; set; }
        [Show, LichSu]
        [Display(Name = "Người dùng đổi SIM")]
        public string NguoiDungDoiSIM { get; set; }
        [Show, LichSu]
        [Display(Name = "Msin cũ")]
        public string MsinCu { get; set; }
        [Show, LichSu]
        [Display(Name = "Msin mới")]
        public string MsinMoi { get; set; }

        public string GoiCuocNguoiTH { get; set; }

        public string GoiCuocThaoTac { get; set; }

        public string GoiCuocMaDV { get; set; }

        public string GoiCuocName { get; set; }

        public string GoiCuocBatDau { get; set; }

        public string GoiCuocKetThuc { get; set; }

        public string NgayINI { get; set; }
        public string GhiChuINI { get; set; }
        public string SoCMT { get; set; }

        #endregion

        #region Method
        public DataResult dataLSNull(DataResult data)
        {
            data.OC = this.nullValue;
            data.IC = this.nullValue;
            data.NgayKhoaOC = this.nullValue;
            data.NgayKhoaIC = this.nullValue;
            data.GhiChuKhoaOC = this.nullValue;
            data.GhiChuKhoaIC = this.nullValue;
            data.NgayCAN = this.nullValue;
            data.GhiChuCAN = this.nullValue;
            data.NgayDoiSIM = this.nullValue;
            data.GhiChuDoiSIM = this.nullValue;
            data.NguoiDungDoiSIM = this.nullValue;
            data.MsinCu = this.nullValue;
            data.MsinMoi = this.nullValue;
            return data;
        }

        public DataResult dataLSEmpty(DataResult data)
        {
            data.OC = this.emptyValue;
            data.IC = this.emptyValue;
            data.NgayKhoaOC = this.emptyValue;
            data.NgayKhoaIC = this.emptyValue;
            data.GhiChuKhoaOC = this.emptyValue;
            data.GhiChuKhoaIC = this.emptyValue;
            data.NgayCAN = this.emptyValue;
            data.GhiChuCAN = this.emptyValue;
            data.NgayDoiSIM = this.emptyValue;
            data.GhiChuDoiSIM = this.emptyValue;
            data.NguoiDungDoiSIM = this.emptyValue;
            data.MsinCu = this.emptyValue;
            data.MsinMoi = this.emptyValue;
            return data;
        }


        public DataResult dataTTNull(DataResult data)
        {
            data.Msin = this.nullValue;
            data.LoaiTB = this.nullValue;
            data.MaTinh = this.nullValue;
            data.NgayKH = this.nullValue;
            data.TenKH = this.nullValue;
            return data;
        }

        public DataResult dataTTEmpty(DataResult data)
        {
            data.Msin = this.emptyValue;
            data.LoaiTB = this.emptyValue;
            data.MaTinh = this.emptyValue;
            data.NgayKH = this.emptyValue;
            return data;
        }


        public DataResult dataTKNull(DataResult data)
        {
            data.TKChinh = this.nullValue;
            data.TKKM = this.nullValue;
            data.TKKM1 = this.nullValue;
            data.TKKM2 = this.nullValue;
            data.HanSD = this.nullValue;
            return data;
        }

        public DataResult dataTKEmpty(DataResult data)
        {
            data.TKChinh = this.emptyValue;
            data.TKKM = this.emptyValue;
            data.TKKM1 = this.emptyValue;
            data.TKKM2 = this.emptyValue;
            data.HanSD = this.emptyValue;
            return data;
        }
        #endregion

        #region Attribute
        /// <summary>
        ///  Attribute prop sử dụng show trong bảng
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public class Show : Attribute
        {

        }
        /// <summary>
        ///  Attribute prop sử dụng nội bộ
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public class NoiBo : Attribute
        {

        }
        /// <summary>
        ///  Attribute prop TK
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public class TaiKhoan : Attribute
        {

        }
        /// <summary>
        ///  Attribute prop LS
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public class LichSu : Attribute
        {

        }



        /// <summary>
        ///  Attribute prop LS
        /// </summary>
        [AttributeUsage(AttributeTargets.Property)]
        public class KhachHang : Attribute
        {

        }
        #endregion


    }
}
