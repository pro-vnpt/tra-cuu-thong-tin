﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tra_cuu_thong_tin.Entity
{
    public class APIExternal<T>
    {
        public string error { get; set; }
        public string error_code { get; set; }
        public string message { get; set; }
        public string message_detail { get; set; }
        public string request_id { get; set; }
        public string page_info { get; set; }
        public List<T> data { get; set; }
    }
}
