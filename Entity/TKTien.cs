﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tra_cuu_thong_tin.Entity
{
    public class TKTien
    {
        #region Property
        public string ID { get; set; }
        public string REMAIN { get; set; }
        public string TIME_END { get; set; }
        #endregion
    }
}
