﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tra_cuu_thong_tin.Entity
{
    public class TTCoBan
    {
        #region Property
        public string GOI_DI { get; set; }
        public string GOI_DEN { get; set; }
        public string IMSI { get; set; }
        public string LOAI_TB { get; set; }
        public string THANG_BD { get; set; }
        public string NGAY_KH { get; set; }
        public string MA_TINH { get; set; }
        public string TEN_KH { get; set; }
        public string TRA_SAU { get; set; }
        public string SO_MSIN { get; set; }
        #endregion
    }
}
