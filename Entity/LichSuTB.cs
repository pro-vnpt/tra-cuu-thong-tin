﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tra_cuu_thong_tin.Entity
{
    public class LichSuTB
    {
        #region Property
        public string NGAY_THANG { get; set; }
		public string NGAY_THANG1 { get; set; }
		public string MA_DV { get; set; }
		public string THAO_TAC { get; set; }
		public string SO_MSIN_CU { get; set; }
        public string SO_MSIN_MOI { get; set; }
		public string NGUOI_DUNG { get; set; }
        public string GHI_CHU { get; set; }
        #endregion


    }
}
